package juego;

import java.awt.Color;
import entorno.Entorno;

public class Pelota {

	private double x;
	private double y;

	private double tamaño; // size
	private Color color;

	private double velocidad;
	private double angulo;

	public Pelota(double x, double y, double velocidad) {
		this.x = x;
		this.y = y;
		this.tamaño = 50;
		this.color = Color.BLUE;
		this.velocidad = velocidad;
		this.angulo = - Math.PI / 4;
	}

	public void dibujar(Entorno e) {
		e.dibujarCirculo(x, y, 50, Color.BLUE);
	}

	public void mover() {
		x += velocidad * Math.cos(angulo);
		y += velocidad * Math.sin(angulo);
	}

	public void acelerar() {
		velocidad += 0.5;
	}

	public boolean chocasteConElEntorno(Entorno entorno) {
		// TODO Auto-generated method stub
		return false;
	}

	public void cambiarDireccion() {
		// TODO Auto-generated method stub
	}

}
