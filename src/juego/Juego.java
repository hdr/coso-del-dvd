package juego;

import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Pelota pelota;
	
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Prueba del Entorno", 800, 600);
		
		// Inicializar lo que haga falta para el juego
		// ...

		this.pelota = new Pelota(entorno.ancho() / 2, entorno.alto() / 2, 1);
		
		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...
		pelota.dibujar(entorno);
		pelota.mover();
		
		if (entorno.estaPresionada('s')) {
			pelota.acelerar();
		}
		
		if (pelota.chocasteConElEntorno(entorno)) {
//			pelota.cambiarDireccion();
			System.out.println("Viva Perón");
		}
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
